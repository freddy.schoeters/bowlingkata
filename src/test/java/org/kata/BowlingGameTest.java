package org.kata;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class BowlingGameTest {

//    @Test
//    public void canRollBall() {
//        BowlingGame bowlingGame = new BowlingGame();
//        bowlingGame.roll(0);
//    }

    private BowlingGame bowlingGame;

    @Before
    public void setUp() throws Exception {
        bowlingGame = new BowlingGame();
    }
/*
    @Test
    public void testShouldScore0ForGutterGame() {

        roll(20, 0);
        assertThat(bowlingGame.score(), is(0));
    }

    @Test
    public void testShouldScore20ForGameOfOnes() {
        roll(20, 1);
        assertThat(bowlingGame.score(), is(20));
        assertEquals(bowlingGame.score(), 20);
    }

    @Test
    public void testShouldScore20ForSpareFollowedBy5AndZeros() {
        roll(1, 8);
        roll(1,2);
        roll(1,5);
        roll(17,0);
        assertThat(bowlingGame.score(), is(20));
    }

    private void roll(int rolls, int pinsDown) {
        for (int i = 0; i < rolls; i++) {
            bowlingGame.roll(pinsDown);
        }
    }
*/

    @Test
    public void testShouldScore0ForGutterGame() {

        bowlingGame.roll(0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0);
        assertThat(bowlingGame.score(), is(0));
    }

    @Test
    public void testShouldScore20ForGameOfOnes() {
        bowlingGame.roll(1,1, 1,1, 1,1, 1,1, 1,1, 1,1, 1,1, 1,1, 1,1, 1,1);
        assertThat(bowlingGame.score(), is(20));
        assertEquals(bowlingGame.score(), 20);
    }

    @Test
    public void testShouldScore20ForSpareFollowedBy5AndZeros() {
        bowlingGame.roll(8,2, 5,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0);
        assertThat(bowlingGame.score(), is(20));
    }

    @Test
    public void testShouldScore22ForStrikeFollowedByFourAndTwoAndGutterballs() {
        bowlingGame.roll(10, 4,2, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0);
        assertThat(bowlingGame.score(), is(22));
    }

    @Test
    public void testShouldScore14ForAllGutterballsEndingWithSpareFollowedByFour() {
        bowlingGame.roll(0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 8,2, 4);
        assertThat(bowlingGame.score(), is(14));
    }

    @Test
    public void testShouldScore16ForAllGutterballsEndingWithStrikeFollowedByFourAndTwo() {
        bowlingGame.roll(0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 10, 4,2);
        assertThat(bowlingGame.score(), is(16));
    }

    @Test
    public void testShouldScore300ForPerfectGame() {
        bowlingGame.roll(10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);
        assertThat(bowlingGame.score(), is(300));
    }
}
