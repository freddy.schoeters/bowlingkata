package org.kata;

public class BowlingGame {

    private int roll = 0;
    private int[] rolls = new int[21];

    public void roll(int pinsDown) {
        rolls[roll++] = pinsDown;
    }

    public void roll(int...rolls) {
        for (int i = 0; i < rolls.length; i++) {
            roll(rolls[i]);
        }
    }

    public int score() {
        int score = 0;
        int rollIndex = 0;
        for (int frame = 0; frame < 10; frame++){
            if (isStrike(rollIndex)) {
                score += 10 + rolls[rollIndex+1] + rolls[rollIndex+2];
                rollIndex++;
            }
            else if (isSpare(rollIndex)) {
                score += 10 + rolls[rollIndex+2];
                rollIndex += 2;
            }
            else {
                score += rolls[rollIndex] + rolls[rollIndex+1];
                rollIndex += 2;
            }
        }
        return score;
    }

    private boolean isStrike(int rollIndex) {
        return rolls[rollIndex] == 10;
    }

    private boolean isSpare(int rollIndex) {
        return rolls[rollIndex] + rolls[rollIndex+1] == 10;
    }
}
